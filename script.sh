#! /bin/sh
#
# script.sh
# Copyright (C) 2018 jacqueline <jacqueline@breakfast.moonstreet.local>
#
# Distributed under terms of the MIT license.
#

rm app* step*

git checkout -b new-feature
touch step1.md
git add .
git commit -m "step 1"
touch step2.md
git add .
git commit -m "step 2"
git checkout master
touch approved1.md
git add .
git commit -m "approved1"
git push origin master
touch approved2.md
git add .
git commit -m "approved2"
git push origin master
git checkout new-feature
touch step3.md
git add .
git commit -m "step 3"
echo "insert bad code here" > step3.md
git add .
git commit -m "bad code"
rm step3.md
touch step3.md
git add .
git commit -m "fixed step 3"
